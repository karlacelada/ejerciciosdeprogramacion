

function sum(base /*, n2, ... */) {
  base = Number(base);
  for (var i = 1; i < arguments.length; i++) {
    base += Number(arguments[i]);
  }
  return base;
}

console.log(sum(3,4,1,2));
console.log(sum(9,1));