

function curry(fn) {
  function nest(num, args) {
    return (...valor) => {
      if (num - valor.length <= 0) {
        return fn(...args, ...valor);
      }
      return nest(num - valor.length, [...args, ...valor]);
    };
  }
  return nest(fn.length, []);
}
const Mult = curry((x, y) => x * y);
console.log(Mult(2,9));
console.log(Mult(4)(6));
